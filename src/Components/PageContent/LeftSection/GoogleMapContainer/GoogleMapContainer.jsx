import React from 'react';
import { compose, withProps } from "recompose";
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
    Polygon
} from "react-google-maps";
import markerIcon from '../../../../assets/Images/Marker.png';
const styles = require('../../../../google_maps_dark_mode.json')
const GoogleMapContainer = compose(
    withProps({
        googleMapURL:
            "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: window.matchMedia("only screen and (max-width:767px").matches ? "200px" : `100%` }} />,
        mapElement: <div style={{ height: `100%`, borderRadius: window.matchMedia("only screen and (max-width:767px").matches ? "0" : "4px" }} />
    }),
    withScriptjs,
    withGoogleMap
)(props => (
    <GoogleMap
        defaultZoom={5}
        defaultCenter={{
            lat: 45.3052400000002, lng: -71.08482
        }}
        defaultOptions={{
            disableDefaultUI: true,
            draggable: true,
            scaleControl: true,
            scrollwheel: true,
            styles: styles
        }}
    >
        {props.isMarkerShown && (
            <Marker
                icon={markerIcon}
                position={{ lat: 45.230135, lng: -69.058598 }}
            />
        )}
        <Polygon
            paths={
                [
                    { lat: 45.137451890638886, lng: -67.13734351262877 },
                    { lat: 44.8097, lng: -66.96466 },
                    { lat: 44.3252, lng: -68.03252 },
                    { lat: 43.98, lng: -69.06 },
                    { lat: 43.68405, lng: -70.11617 },
                    { lat: 43.090083319667144, lng: -70.64573401557249 },
                    { lat: 43.08003225358635, lng: -70.75102474636725 },
                    { lat: 43.21973948828747, lng: -70.79761105007827 },
                    { lat: 43.36789581966826, lng: -70.98176001655037 },
                    { lat: 43.46633942318431, lng: -70.94416541205806 },
                    { lat: 45.3052400000002, lng: -71.08482 },
                    { lat: 45.46022288673396, lng: -70.6600225491012 },
                    { lat: 45.914794623389355, lng: -70.30495378282376 },
                    { lat: 46.69317088478567, lng: -70.00014034695016 },
                    { lat: 47.44777598732787, lng: -69.23708614772835 },
                    { lat: 47.184794623394396, lng: -68.90478084987546 },
                    { lat: 47.35462921812177, lng: -68.23430497910454 },
                    { lat: 47.066248887716995, lng: -67.79035274928509 },
                    { lat: 45.702585354182816, lng: -67.79141211614706 },
                    { lat: 45.137451890638886, lng: -67.13734351262877 },
                ]}
            options={{
                strokeColor: '#FF2E79',
                strokeOpacity: 1,
                strokeWeight: 2,
                fillColor: '#FF2E79',
                fillOpacity: 0.15
            }}
        />
    </GoogleMap>
));
export default GoogleMapContainer;