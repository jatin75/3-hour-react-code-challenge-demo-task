import React from 'react';
import './MiddleSection.css';
import agency from '../../../assets/Images/Agency.png';
import fileIcon from '../../../assets/Images/common-file-text.png';
import ImgCarousel from './ImgCarousel';
class MiddleSection extends React.Component {
    state = {
        showImgCarousel: false
    }
    componentDidMount = () => {
        // Check for device 
        let isMobile = window.matchMedia("only screen and (max-width:767px").matches;
        isMobile && this.setState({ showImgCarousel: true })
    }
    render() {
        let { showImgCarousel } = this.state;
        return (
            <>
                {/* Bar in mobile view */}
                <div className="bar"></div>
                {/* Page extreme alert section */}
                <div className="extreme-alert">
                    <div className="left">
                        <div className="title">
                            Extereme Alert
                        </div>
                        <div className="distance">
                            ~ 2 miles away
                        </div>
                    </div>
                    <div className="right">
                        <div className="time">
                            6 hours ago
                        </div>
                    </div>
                </div>

                {/* Middle section main title */}
                <div className="main-title"> Help us find missing 14 year old Savvanah Smith last seen at Zilker Park</div>
                {/* Agency */}
                <img src={agency} alt="agency-logo" />

                {
                    showImgCarousel &&
                    <ImgCarousel />
                }
                {/* Details section */}
                <div className="details">
                    <div className="details-title">Details</div>
                    <div className="details-desc">Please help us find missing 14 year old Savvanah Smith who was last seen with her friends at Zilkder Park on Monday August 21st at 4:45pm. Savvanah was wearing a red sweatshirt and plack pants and has not been seen for over 1 day.</div>
                </div>
                {/* Instruction section */}
                <div className="details pt-0">
                    <div className="details-title">Intructions</div>
                    <div className="details-desc">Here are the instrucitons that can be included. Please call 911 for more information.</div>
                </div>
                {/* Attachments section */}
                <div className="attachments">
                    <div className="details-title">Attachments</div>
                    <div className="attachment-file">
                        <span><img src={fileIcon} alt="file-icon" /></span>
                        <span className="pl-2">Missing_flyer.pdf</span>
                    </div>
                </div>
            </>
        )
    }
}
export default MiddleSection;