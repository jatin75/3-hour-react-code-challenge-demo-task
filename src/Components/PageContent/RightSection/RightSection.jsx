import React from 'react';
import image1 from '../../../assets/Images/image 2.png';
import image2 from '../../../assets/Images/image 3.png';
import image3 from '../../../assets/Images/image 4.png';
import image4 from '../../../assets/Images/image 5.png';
import runaway from '../../../assets/Images/runaway.png';
import './RightSection.css';
import $ from 'jquery';
class RightSection extends React.Component {
    state = {
        carouselImages: [
            {
                id: 1,
                thumbnail: image1,
                image: runaway
            },
            {
                id: 2,
                thumbnail: image2,
                image: image2
            },
            {
                id: 3,
                thumbnail: image3,
                image: image3
            },
            {
                id: 4,
                thumbnail: image4,
                image: image4
            },
        ],
        activeImage: {
            id: 1,
            thumbnail: image1,
            image: runaway
        },
        updatesArray: [
            {
                id: 1,
                duration: "7 mins ago",
                update: "Here is an update that will show up if admins post an update from the web portal."
            },
            {
                id: 2,
                duration: "32 mins ago",
                update: "Here is a new update that will show up if admins post something"
            },
        ]
    }
    componentDidMount = () => {
        $(".wide-image img").css("height",
            $(".wide-image").width()
        );
    }
    handleImageClick = (imageInfo) => {
        this.state.activeImage.id !== imageInfo.id &&
            this.setState({
                activeImage: imageInfo
            })
    }
    render() {
        let { carouselImages, activeImage, updatesArray } = this.state;
        return (
            <>
                {/* Custom images view with thumbnails */}
                <div className="image-carousel">
                    <div className="wide-image">
                        <img src={activeImage.image} alt={"img_" + activeImage.id} id={"id_" + activeImage.id} />
                    </div>
                    <div className="img-thumbnails">
                        {
                            carouselImages.map((imageInfo, index) => (
                                <img
                                    className={activeImage.id === imageInfo.id ? "active" : ""}
                                    src={imageInfo.thumbnail}
                                    key={index}
                                    alt={"img_" + index}
                                    onClick={() => this.handleImageClick(imageInfo)}
                                />
                            ))
                        }
                    </div>
                </div>
                {/* Updates section */}
                <div className="updates-container">
                    <div className="updates-header">Updates</div>
                    {updatesArray.map((update, index) => (
                        <div className="update" key={index}>
                            <div className="timeline-dot"></div>
                            <div className="duration">
                                {update.duration}
                            </div>
                            <div className="update-desc">
                                {update.update}
                            </div>
                        </div>
                    ))}
                </div>
            </>
        )
    }
}
export default RightSection;