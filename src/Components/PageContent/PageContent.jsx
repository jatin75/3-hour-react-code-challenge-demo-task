import React from 'react';
import LeftSection from './LeftSection';
import MiddleSection from './MiddleSection';
import RightSection from './RightSection';
import './PageContent.css';
class PageContent extends React.Component {
    render() {
        return (
            <div className="page-content">
                <div className="left-section">
                    <LeftSection />
                </div>
                <div className="mobile-overlay">
                    <div className="middle-section">
                        <MiddleSection />
                    </div>
                    <div className="right-section">
                        <RightSection />
                    </div>
                </div>
            </div>
        )
    }
}
export default PageContent;