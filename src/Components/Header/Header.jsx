import React from 'react';
import './Header.css';
import logo from '../../assets/Images/white-blue.png';
class Header extends React.Component {
    render() {
        return (
            <header className="row align-items-center header">
                <div className="col-6 text-left p-0">
                    <img src={logo} alt="app-logo" width="200px" height="44px" />
                </div>
                <div className="col-6 text-right p-0">
                    <button className="btn get-app-btn">GET THE APP</button>
                </div>
            </header>
        )
    }
}
export default Header;