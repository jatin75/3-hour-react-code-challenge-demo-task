import React from 'react';
import Header from './Components/Header';
import PageContent from './Components/PageContent';

function App() {
  return (
    // Main page wrapper 
    <>
      <Header />
      <PageContent />
    </>
  );
}

export default App;
