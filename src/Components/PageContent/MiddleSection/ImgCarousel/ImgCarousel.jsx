import React from 'react';
// import 'owl-carousel/owl.theme.css';
import OwlCarousel from 'react-owl-carousel2';
import 'react-owl-carousel2/src/owl.carousel.css';
import 'react-owl-carousel2/src/owl.theme.default.css';
import image2 from '../../../../assets/Images/image 3.png';
import image3 from '../../../../assets/Images/image 4.png';
import image4 from '../../../../assets/Images/image 5.png';
import runaway from '../../../../assets/Images/runaway.png';
const options = {
    // items: 1,
    dots: true,
    // nav: true,
    // rewind: true,
    autoplay: true,
    stageOuterClass: "img-carousel",
    loop: true,
    margin: 10,
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 3,
            nav: false
        },
        1000: {
            items: 5,
            // nav: true,
            loop: false
        }
    }
};

class ImgCarousel extends React.Component {
    render() {
        return (
            <OwlCarousel ref="car" options={options} >
                <div><img src={image2} alt="The Last of us" /></div>
                <div><img src={image3} alt="GTA V" /></div>
                <div><img src={image4} alt="Mirror Edge" /></div>
                <div><img src={runaway} alt="Mirror Edge" /></div>
            </OwlCarousel>
        )
    }
}
export default ImgCarousel;