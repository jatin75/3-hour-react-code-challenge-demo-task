import React from 'react';
import './LeftSection.css';
import GoogleMapContainer from './GoogleMapContainer';

class LeftSection extends React.Component {
    render() {
        return (
            // Google map with polygon and custom marker
            <GoogleMapContainer isMarkerShown={true} />
        )
    }
}
export default LeftSection;